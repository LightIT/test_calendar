Rails.application.routes.draw do
  resources :calendars, only: [:index] do
    collection do
      get :callback
      get :events
    end
  end
end
