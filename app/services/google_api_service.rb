require 'signet/oauth_2/client'

class GoogleApiService
  attr_reader :client

  def initialize(authorization = nil)
    @client = Signet::OAuth2::Client.new(options)
    @client.update!(authorization) if authorization
  end

  def authorization_uri
    client.authorization_uri.to_s
  end

  protected

  def options
    {
      client_id: Rails.application.secrets.google_client_id,
      client_secret: Rails.application.secrets.google_client_secret,
      authorization_uri: 'https://accounts.google.com/o/oauth2/auth',
      token_credential_uri: 'https://accounts.google.com/o/oauth2/token',
      scope: nil,
      redirect_uri: nil
    }
  end
end