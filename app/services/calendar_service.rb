require 'google/apis/calendar_v3'

class CalendarService < GoogleApiService
  def list
    calendar_service.list_calendar_lists
  end

  def events(calendar_id, options)
    options = {
      time_min: options[:time_min]&.to_time&.rfc3339,
      time_max: options[:time_max]&.to_time&.rfc3339
    }
    calendar_service.list_events(calendar_id, options)
  end

  private

  def calendar_service
    service = Google::Apis::CalendarV3::CalendarService.new
    service.authorization = client
    service
  end

  protected

  def options
    opt = super
    opt[:scope] = Google::Apis::CalendarV3::AUTH_CALENDAR
    opt[:redirect_uri] = Rails.application
                              .routes
                              .url_helpers
                              .callback_calendars_url(host: 'localhost:3000')
    opt
  end
end