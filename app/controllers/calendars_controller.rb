class CalendarsController < ApplicationController
  before_action :authorize_calendar, except: :callback

  rescue_from Google::Apis::AuthorizationError do
    session[:calendar_access_token] = nil
    authorize_calendar
  end

  def index
    @calendar_list = calendar_service.list
  end

  def events
    @events = calendar_service.events(params[:calendar_id], event_params)
  end

  def callback
    client = CalendarService.new.client
    client.code = params[:code]
    response = client.fetch_access_token!
    session[:calendar_access_token] = response

    redirect_to calendars_url
  end

  private

  def authorize_calendar
    return if session[:calendar_access_token].present?
    redirect_to CalendarService.new.authorization_uri
  end

  def calendar_service
    CalendarService.new(session[:calendar_access_token])
  end

  def event_params
    params.permit(:time_min, :time_max)
  end
end